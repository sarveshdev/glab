
from typing import Sequence
import sys

def get_sum(numbers:Sequence[int])->int:
    """
    Returns the sum of the numbers in the sequence.
    """
    return sum(numbers)

if __name__ == '__main__':
    print(get_sum([int(num) for num in sys.argv[1:]]))
